# Meili agent ROS2

## Prerequisites. 

1. Common dependencies

```
sudo apt-get update -y
sudo apt-get install git
```

2. Required python packages
```
sudo apt-get install python3-pip -y
sudo -H pip3 install --upgrade pip
sudo pip install setuptools==58.2.0
sudo apt install python3-colcon-common-extensions
sudo apt-get install python3-rosdep
sudo apt-get install gnome-terminal -y
```

3. Download meili-cli tool to clone this repository with the fleet setup desired: 
```
curl https://meili-fms-dev.s3.eu-north-1.amazonaws.com/helpers/meili-cli-386 --output meili-cli
chmod +x meili-cli
```

## Installation
1. Create and build a ROS2 workspace if not created: 
```
source /opt/ros/humble/setup.bash
mkdir -p ~/ros2_ws_meili/src
cd ~/ros2_ws_meili
colcon build
```

2. Save ROS_PACKAGE_PATH environment variable to clone later the meili agent
```
cd ~/ros2_ws_meili
export LAST_COLCON_PATH=`pwd`'/src'
export ROS_PACKAGE_PATH=$LAST_COLCON_PATH:$ROS2_PACKAGE_PATH
source ~/ros2_ws_meili/install/setup.bash
```

3. Generate a fleet setup in Meili FMS and get the PIN by following this tutorial: https://docs.meilirobots.com/docs/get-started/add-vehicles/ros-setup/

4. Download meili-agent by running meili-cli tool with the PIN generated previously: 
```
./meili-cli init -pin PIN -site SITE
./meili-cli setup
```

5. Install python dependencies
```
cd ~/ros2_ws_meili/src/meili-agent/meili_agent/meili_agent
sudo -H pip3 install --ignore-installed PyYAML
sudo -H pip3 install -r requirements.txt
sudo apt-get update
cd ~/ros2_ws_meili/
source /opt/ros/humble/setup.bash
source ~/ros2_ws_meili/install/setup.bash
```

6. Update rosdep and install Meili agent ros dependencies
```
sudo rosdep init
rosdep update
rosdep install --from-paths src --ignore-src --rosdistro humble -y -r
```

7. Finally, build the ROS2 workspace
```
colcon build
source ~/ros2_ws_meili/install/setup.bash
```

## Parameters
- publish_frequency: Please modify the publication frequency to suit your needs, with a minimum value of 1 Hz.
```
export publish_frequency=1
```

- topic_streaming_mode: There are two different modes that exist:
    - FILTERING:  Default value. Streams topics in a determined frequency.
    - REAL_TIME: Streams topics in real-time.
```
export topic_streaming_mode=FILTERING
```

- battery_present: Indicates if battery values are available and if the user wants to stream it to the cloud. Values can be True or False. 
```
export battery_present=False
```

- path_planning: When allocating tasks with multiple points, the user can choose to use a route calculated by Meili FMS’s path planner. True or False. 
```
export path_planning=True
```

- traffic_control: Indicates if real-time paths calculated by AMRs are streamed to the cloud, and traffic control algorithms will be applied.
    - True: Traffic control algorithms will be applied when more than one task is on.
    - False: Default value. No path will be streamed and no traffic control algorithms.
```
export traffic_control=False
```

- offline: Indicates if offline mode is activated in the agent.
    - True:  The Meili Agent will periodically check the wifi connectivity. 
    - False:  Default value. Wifi connection will not be checked periodically. 
```
export offline=False
```

- lipo_cells: Indicates whether the battery is a LiPo battery or not. If >0 the battery is a LiPo battery.
```
export lipo_cells=0
```


## Running
If you want to launch the meili agent with the default parameters (the ones defined in the previous section), run the following command:

```
ros2 launch meili_agent meili_agent.launch
```

In case some parameters want to be changed, define the parameter as an environment variable as explained in the previous section and run the meili agent including the variables desired: 

```
ros2 launch meili_agent meili_agent.launch parameter1:=$parameter1 parameter2=:parameter2
```

Where parameter1 and parameter2 are the name of the variables defined in the previous section. You can include in the ros2 launch command as many parameters as you need. 

## Formatting

1. Install pre-commit from https://pre-commit.com/

2. Run `pre-commit install`

3. Happy committing!
