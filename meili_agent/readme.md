# Meili agent ROS2 Foxy

## Prerequisites. 

1. Python3-pip:

```
sudo apt-get install python3-pip
```

2. Colcon

```
sudo apt install python3-colcon-common-extensions
```

3. Rosdep

```
sudo apt-get install python3-rosdep
``` 

## Installation

1. Clone repository in your ros2_ws inside src folder

2. Install python dependencies

```
cd ros2_ws/src/meili-agent/meili-agent-ros2/meili_agent

pip install -r requirements.txt
```

3. Install ros dependencies

```
source /opt/ros/foxy/setup.bash
cd ros2_ws
rosdep install --from-paths src --ignore-src --rosdistro foxy -y -r
```

4. Set up config file.

	1. Create config.yaml file inside config folder

		```
		cd meili-agent/meili-agent-ros2/config

		touch config.yaml

		```

	2. Config file contain information about the fleet, specific vehicles and MQTT auth. This information need to be configured using Meili Cloud. An example of config file:

```
vehicles:

	uuid: bae59388be704a6e89e5c8a7907b74243
	token:71876edfe6ea939e696f85158c0c7516187136e94
	prefix:

fleet: df1d4b9df56fe55fc43e34e16e533d3df050fa86
mqqt_id: meili-agent-65e49378-f100-5311-918a-11f18ac51fbb
```

5. Rebuild ros enviroment:

```
cd ros2_ws
colcon build
source install/setup.bash
```

## Running

```
source /opt/ros/foxy/setup.bash
cd ~/ros2_foxy
source install/setup.bash
ros2 launch meili_agent meili_agent.launch.py
```