import os
from glob import glob
from setuptools import setup, find_packages

package_name = "meili_agent"
lib_name = "meili_ros_lib"
setup(
    name=package_name,
    version="0.0.0",
    packages=[package_name],
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        (os.path.join('share', package_name), glob('launch/*launch.[pxy][yma]*')),
        ("share/" + package_name, ["meili_agent/data/defaults.json"]),
        ("share/" + package_name, ["config/config.yaml"]),

    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="juan",
    maintainer_email="juan@meilirobots.com",
    description="TODO: Package description",
    license="TODO: License declaration",
    entry_points={
        "console_scripts": [
            "agent_topic_streaming = meili_agent.meili_agent_topic_streaming:main",
            "agent_node = meili_agent.agent_node:main",
            "docking_server = meili_agent.docking_server:main",

        ],
    },
)
