from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
    influx_bucket = LaunchConfiguration("influx_bucket")
    influx_org = LaunchConfiguration("influx_org")
    influx_token = LaunchConfiguration("influx_token")
    influx_url = LaunchConfiguration("influx_url")

    # ---------
    publish_frequency = LaunchConfiguration("publish_frequency")
    traffic_control = LaunchConfiguration("traffic_control")
    battery_present = LaunchConfiguration("battery_present")
    lipo_cells = LaunchConfiguration("lipo_cells")
    path_planning = LaunchConfiguration("path_planning")
    offline = LaunchConfiguration("offline")
    v2 = LaunchConfiguration("v2")
    logging = LaunchConfiguration("logging")

    # ---------

    declare_influx_bucket = DeclareLaunchArgument(
        "influx_bucket", default_value="topics"
    )
    declare_influx_org = DeclareLaunchArgument(
        "influx_org", default_value="meili-demo"
    )
    declare_influx_token = DeclareLaunchArgument(
        "influx_token",
        default_value="yc1roxaaL4G30dmbByv2r7PLk2f9d7Uho5bflv8w0TLGyYhVbNEV-gi2gKuJBrJqhaab45FvmuosQyBc2ADOPA==",
    )
    declare_influx_url = DeclareLaunchArgument(
        "influx_url", default_value="https://influx-development.meilirobots.com"
    )
    # ---------
    declare_publish_frequency = DeclareLaunchArgument(
        "publish_frequency", default_value="1"
    )
    declare_traffic_control = DeclareLaunchArgument(
        "traffic_control", default_value="False"
    )
    declare_battery_present = DeclareLaunchArgument(
        "battery_present", default_value="False"
    )
    declare_lipo_cells = DeclareLaunchArgument(
        "lipo_cells", default_value="0"
    )
    declare_path_planning = DeclareLaunchArgument(
        "path_planning", default_value="True"
    )
    declare_offline_flag = DeclareLaunchArgument(
        "offline", default_value="False"
    )
    declare_v2 = DeclareLaunchArgument(
        "v2", default_value="True"
    )
    declare_logging = DeclareLaunchArgument(
        "logging", default_value=""
    )

    # ---------
    meili_agent_topic_streaming = Node(
        package="meili_agent",
        executable="agent_topic_streaming",
        output="screen",
        name="meili_agent_topic_streaming",
        parameters=[
            {"publish_frequency": publish_frequency},
            {"influx_bucket": influx_bucket},
            {"influx_org": influx_org},
            {"influx_token": influx_token},
            {"influx_url": influx_url},
        ],
    )
    agent = Node(
        package="meili_agent",
        executable="agent_node",
        output="screen",
        name="agent_node",
        parameters=[
            {"publish_frequency": publish_frequency},
            {"traffic_control": traffic_control},
            {"battery_present": battery_present},
            {"lipo_cells": lipo_cells},
            {"path_planning": path_planning},
            {"offline": offline},
            {"v2": v2},
            {"logging": logging},


        ],
    )

    docking = Node(
        package="meili_agent",
        executable="docking_server",
        output="screen",
        name="docking_server"
    )
    # ---------
    ld = LaunchDescription()

    ld.add_action(declare_publish_frequency)
    ld.add_action(declare_traffic_control)
    ld.add_action(declare_battery_present)
    ld.add_action(declare_lipo_cells)
    ld.add_action(declare_path_planning)
    ld.add_action(declare_offline_flag)
    ld.add_action(declare_v2)
    ld.add_action(declare_logging)


    ld.add_action(declare_influx_bucket)
    ld.add_action(declare_influx_org)
    ld.add_action(declare_influx_token)
    ld.add_action(declare_influx_url)

    ld.add_action(agent)
    ld.add_action(docking)

    #ld.add_action(meili_agent_topic_streaming)

    return ld
