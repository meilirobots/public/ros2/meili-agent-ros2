"""Creates ROS2 agent_node and calls the agent and handlers"""
import threading
# ROS2 dependencies
import rclpy
from rclpy.node import Node

# Meili agent dependencies
from meili_agent.clients import setup_clients
from meili_agent.logging import Logging

from meili_agent.agent import MeiliAgent
from meili_agent.handlers import SDKHandlers
from meili_agent.publishers_subscribers import pub_and_sub
from sentry_sdk import capture_exception


from meili_ros_lib.sentry import initialize_sentry
from meili_ros_lib.agent_setup import Setup
from meili_ros_lib.connection import Connection

initialize_sentry()


class InitiationNode(Node, Setup):
    """Class for creating ROS2 node."""

    def __init__(self):
        # Node initialization
        Node.__init__(self, "agent")

        # General Setup
        Setup.__init__(self)

        # ROS2
        # Logging
        logging = Logging(self)
        self.log_info = logging.log_info
        self.log_error = logging.log_error

        self.init_parameters_setup()
        self.get_launch_parameters()

    def get_launch_parameters(self):
        try:
            # ROS2
            self.declare_parameters(
                namespace='',
                parameters=[
                    ('publish_frequency', 1),
                    ('traffic_control', False),
                    ('battery_present', False),
                    ('lipo_cells', 0),
                    ('path_planning', False),
                    ('offline', False),
                    ('v2', True),
                    ('logging', "")
                    ])

            pf, tf, bt, lp, pp, of, v2, lg = self.get_parameters(
                ['publish_frequency', 'traffic_control',
                 'battery_present', 'lipo_cells', 'path_planning', 'offline', 'v2', 'logging']
            )

            self.path_planning = pp.value
            self.traffic_control = tf.value
            battery = bt.value
            lipo_cells = lp.value
            self.publish_frequency = pf.value
            self.offline_flag = of.value
            self.v2 = v2.value
            self.logging = lg.value

            self.battery_parameter(battery, lipo_cells)

            self.log_info(f"\n----------------------------\n"
                          f"Path Planning = {pp.value}\n"
                          f"Traffic Control = {tf.value}\n"
                          f"Battery = {bool(bt.value or lp.value)}\n"
                          f"Publish Frequency = {pf.value}\n"
                          f"Offline Flag = {of.value}\n"
                          f"Task V2 = {v2.value}\n"
                          f"Logging = {lg.value}\n"
                          f"----------------------------")


        except KeyError:
            self.log_error("[ROSAgent] Parameter %s does not exist. ", KeyError)
            capture_exception(KeyError)
            raise


def main(args=None):
    rclpy.init(args=args)
    ################################################
    # SETUP
    node = InitiationNode()
    agent = MeiliAgent(node)
    handlers = SDKHandlers(agent)
    agent.connection = Connection(
        node, handlers,
        agent.vehicle_list,
    )
    ################################################
    # ROS2 CLIENTS
    agent.client_list, agent.client_waypoints_list, agent.req_get, agent.cli_get, agent.cli_set, agent.req_set = setup_clients(
        node, agent.vehicles)
    # ROS1 SUBSCRIBERS AND PUBLISHERS
    pub_and_sub(node, agent)
    ################################################

    node.log_info("[ROSAgentNode] Meili Agent is ready for receiving tasks")

    thread = threading.Thread(
        target=rclpy.spin, args=(agent.node,), daemon=True
    )
    thread.start()
    rate = node.create_rate(1)
    pub_rate = node.create_rate(node.publish_frequency)
    check_threads_rate = node.create_rate(1)
    agent.run(rate, pub_rate, check_threads_rate)


    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    agent.node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    try:
        main()
    except Exception as error:
        capture_exception(error)
