import time
import typing as t

from nav2_msgs.action import NavigateToPose, FollowWaypoints
from rcl_interfaces.srv import GetParameters, SetParameters
from rclpy.action import ActionClient
from sentry_sdk import capture_exception


def setup_clients(node, vehicles):
    client_list = client_navigation(NavigateToPose, "/navigate_to_pose", vehicles, node)
    client_waypoints_list = None
    req_get, cli_get, cli_set, req_set = None, None, None, None

    if node.path_planning:
        client_waypoints_list = client_navigation(
            FollowWaypoints, "/follow_waypoints", vehicles, node
        )

    req_get, cli_get = client_parameters(
        "/controller_server/get_parameters", GetParameters,
        vehicles, node)
    req_set, cli_set = client_parameters(
        "/controller_server/set_parameters", SetParameters,
        vehicles, node)

    return client_list, client_waypoints_list, req_get, cli_get, cli_set, req_set


def client_navigation(action, topic, vehicles, node) -> t.List:
    """Returns: Client List"""
    client_list = []
    try:
        for index, vehicle in enumerate(vehicles):
            if vehicle["prefix"]:
                name = vehicle["prefix"] + topic
            else:
                name = topic
            node.get_logger().info(
                "[ROSAgent] Name of client created: %s" % (name,)
            )
            # ROS2
            client_list.append(ActionClient(node, action, name))
            client_list[index].wait_for_server()
        time.sleep(2)
    except KeyError as error:
        node.get_logger().error(
            f"[ROSAgent] Client Navigation Key Error {error}"
        )
        capture_exception(error)
    except Exception as error:
        node.get_logger().error(f"[ROSAgent] Client Navigation Error {error}")
        capture_exception(error)
    return client_list


def client_parameters(param_srv_name, param_class, vehicles, node):
    cli = []
    req = []
    try:
        for index, vehicle in enumerate(vehicles):
            if vehicle["prefix"]:
                name = vehicle["prefix"] + param_srv_name
            else:
                name = param_srv_name  # topic is not defined here

            cli.append(node.create_client(param_class, name))
            node.get_logger().info(
                "[ROSAgent] Parameter service client created %s" % (name,)
            )

            while not cli[index].wait_for_service(timeout_sec=1.0):
                node.get_logger().info(
                    "[ROSAgent] Get service not available, waiting again..."
                )
            req.append(param_class.Request())
        return req, cli

    except KeyError as error:
        node.get_logger().error(
            f"[ROSAgent] Parameter service Client Key Error {error}"
        )
        capture_exception(error)
