import csv
import json
import logging
import os
import threading
from datetime import datetime
from os.path import exists, expanduser, isfile, join
from time import sleep
from typing import Union

import requests
import websocket
from sentry_sdk import capture_exception, capture_message


class WS:
    """
    Websocket class for handling websocket connections
    """

    def __init__(
        self,
        server_instance,
        setup_token,
        parameter_store,
        token: str,
        fleet=False,
        vehicles=[],
    ):
        self.__connection: websocket.WebSocketApp = None
        self.__token = token
        self.__fleet = fleet
        self.thread: threading.Thread = None
        self.task_handler = None
        self.topic_handler = None
        self.error_handler = None
        self.close_handler = None
        self.open_handler = None
        self.topic_request_handler = None
        self.task_cancellation_handler = None
        self.waypoints_handler = None
        self.parameter_store = parameter_store
        self.server_instance = server_instance
        self.setup_token = setup_token

        if not isinstance(vehicles, list):
            capture_exception(TypeError)
            capture_message("[WS] Vehicles should be a list")
            raise TypeError("[WS] Vehicles should be a list")

        self.__vehicles = vehicles

    def start(self, connection_url: str = None):
        """
        Entrypoint to spin up the connection

        Override connection_url by passing it as kwarg
        """
        if self.is_running() and self.__connection is not None:
            capture_message("[WS] Websocket already started")
            raise Exception("[WS] Websocket already started")

        if connection_url is None:
            connection_url = self.__get_conn_url()

        if self.task_handler is None:
            capture_message("[WS] Task_handler is not set")
            raise Exception("[WS] Rask_handler is not set")
        elif not hasattr(self.task_handler, "__call__"):
            capture_message("[WS] Task_handler is not a function")

            raise Exception("[WS] Task_handler is not a function")
        try:
            self.thread = threading.Thread(target=self.__thread, args=[connection_url])
            self.thread.start()
        except Exception as e:
            capture_exception(e)

    def __get_conn_url(self):
        self.parameter_store.env = "dev-public"
        # url = (self.parameter_store).get('/api_host')
        url = "https://" + str((self.server_instance.value)) + ".meilirobots.com/"
        url = url.replace("https", "wss") + "ws/{0}/"
        try:
            return url.format("fleets" if self.__fleet else "vehicles")
        except Exception as e:
            capture_exception(e)

    def add_vehicle(self, vehicle):
        """
        Add vehicle to the list
        """
        if not isinstance(vehicle, str):
            capture_exception(TypeError)
            capture_message("[WS] Vehicle must be of type string")
            raise TypeError("[WS] Vehicle must be of type string")
        self.__vehicles.append(vehicle)

    def get_vehicles(self):
        """
        Return current vehicles on the fleet
        """
        return self.__vehicles

    def set_vehicles(self, vehicles: list):
        """
        Bulk set vehicles
        """
        if not isinstance(vehicles, list):
            capture_message("[WS] Vehicles must be of type list")
            capture_exception(TypeError)
            raise TypeError("[WS] Vehicles must be of type list")

        for vehicle in vehicles:
            if not isinstance(vehicle, str):
                capture_message("[WS] Vehicles must be a list of strings")
                capture_exception(TypeError)
                raise TypeError("[WS] Vehicles must be a list of strings")

        self.__vehicles = vehicles

    def __thread(self, connection_url):
        self.__connection = websocket.WebSocketApp(
            connection_url,
            on_message=self.__on_message,
            on_open=self.__on_open,
            on_error=self.__on_error,
            on_close=self.__on_close,
            header={
                "x-meili-app-name": "ros2",
                "x-meili-app-version": "1.0.1",
                "x-meili-fleet-token": self.__token,
            },
        )

        while True:
            self.__connection.run_forever()
            sleep(5)

    def __on_message(self, _, message):
        try:
            raw_data = json.loads(message)
        except:
            return

        msg_type = raw_data.get("type", None)
        data = raw_data.get("data", None)
        vehicle = raw_data.get("vehicle", None)
        if vehicle is None and self.__fleet:
            return
        if msg_type is None and data is None:
            return

        if msg_type == "move":
            x = data["location"]["x"]
            y = data["location"]["y"]
            xm = data["location"]["metric"]["x"]
            ym = data["location"]["metric"]["y"]
            rotation = data["location"]["rotation"]

            self.task_handler(
                vehicle=vehicle, x=x, y=y, xm=xm, ym=ym, rotation=rotation
            )

        if msg_type == "move_charge":
            data = raw_data
            x = data["location"]["x"]
            y = data["location"]["y"]
            xm = data["location"]["metric"]["x"]
            ym = data["location"]["metric"]["y"]
            rotation = "0.00"  # data["location"]["rotation"]
            self.task_handler(
                vehicle=vehicle, x=x, y=y, xm=xm, ym=ym, rotation=rotation
            )
        if msg_type == "waypoints":

            x = data["location"]["x"]
            y = data["location"]["y"]
            xm = data["location"]["metric"]["x"]
            ym = data["location"]["metric"]["y"]
            rotation = data["location"]["rotation"]

            self.waypoints_handler(
                vehicle=vehicle, x=x, y=y, xm=xm, ym=ym, rotation=rotation
            )

        if msg_type == "task":
            x = data["location"]["x"]
            y = data["location"]["y"]
            xm = data["location"]["metric"]["x"]
            ym = data["location"]["metric"]["y"]
            rotation = data["location"]["rotation"]

            self.task_handler(
                vehicle=vehicle, x=x, y=y, xm=xm, ym=ym, rotation=rotation
            )
        elif msg_type == "request_topics":
            if self.topic_request_handler is not None and hasattr(
                self.topic_request_handler, "__call__"
            ):
                self.topic_request_handler(vehicle=vehicle)

        elif msg_type == "topic_init":
            if self.topic_handler is not None and callable(self.topic_handler):
                self.topic_handler(topics=data, vehicle=vehicle)

        elif msg_type == "drop":
            # goal_id = raw_data.get("goal_id", None)
            goal_id = None
            vehicle_id = raw_data.get("vehicle")
            self.task_cancellation_handler(vehicle=vehicle, goal_id=goal_id)

            if self.topic_handler is not None and hasattr(
                self.topic_handler, "__call__"
            ):
                self.topic_handler(topics=data, vehicle=vehicle)

    def __on_open(self, _):
        logging.info("Websocket connection open")
        if self.__fleet:
            self.__connection.send(
                json.dumps({"event": "register", "vehicles": self.__vehicles})
            )

        if self.open_handler is not None:
            self.open_handler(_)

    def __on_close(self, *_):
        logging.warning("[WS] Websocket connection closed")
        capture_message("[WS] Websocket connection closed")
        if self.close_handler is not None:
            self.close_handler()

    def __on_error(self, connection, error):
        logging.error(
            "[WS] An error occured on websocket connection: {}".format(str(error))
        )
        capture_message("[WS] An error occured on websocket connection")
        if self.error_handler is not None:
            self.error_handler(connection, error)

    def close(self):
        self.__connection.close()

    def is_running(self):
        """
        Check if the connection thread is still running
        """
        return self.thread is not None and self.thread.is_alive()

    def send(self, data: Union[str, dict]):
        """
        Send data to the server. No checks will be done to ensure message validity
        """
        if self.__connection is None:
            capture_message("[WS] Connection is not established")
            raise Exception("[WS] Connection is not established")

        if not isinstance(data, str):
            data = json.dumps(data)

        self.__connection.send(data=data)

    def __check_and_send(self, data, vehicle: str = None):
        """
        Internal method for checking connection and appending vehicle uuid to the message
        """
        if self.__connection is None:
            capture_message("[WS] Connection is not established")
            raise Exception("[WS] Connection is not established")

        if self.__fleet:
            #  if vehicle:
            #     raise ValueError('vehicle is null when connection is fleet')
            data["vehicle"] = vehicle
        self.__connection.send(json.dumps(data))

    def send_location(self, x, y, rotation, vehicle: str = None):
        """
        Send helper to send location to the server
        """
        data = {"event": "location", "value": {"xm": x, "ym": y, "rotation": rotation}}

        self.__check_and_send(data, vehicle=vehicle)

    def send_goal_status(
        self,
        goal_id,
        status_id,
        status_text: str = None,
        vehicle: str = None,
        topic: str = None,
    ):
        """
        Send helper to send goal status data to the server
        """
        data = {
            "event": "goalStatus",
            "value": {"goal_id": goal_id, "status_id": status_id},
        }

        if topic:
            data["topic"] = topic
        if status_text:
            data["status_text"] = status_text
        self.__check_and_send(data, vehicle=vehicle)

    def send_topic_data(
        self, topic: str, message_type: str, topic_data: dict, vehicle: str = None
    ):
        """
        Send helper to send topic data to the server
        """
        data = {
            "event": "topic_data",
            "topic": topic,
            "message_type": message_type,
            "data": {**topic_data},
        }
        self.__check_and_send(data, vehicle=vehicle)

    def send_battery(self, battery_data: dict, topic: str = None, vehicle: str = None):
        """
        Send helper for reporting battery status back to the server
        """

        data = {"event": "batteryStatus", "value": {**battery_data}}

        if topic is not None:
            data["topic"] = topic

        self.__check_and_send(data, vehicle=vehicle)

    def send_path(self, path, vehicle: str = None):
        """
        Send helper to send location to the server
        """
        data = {"event": "path_data", "value": {"path": path}}
        self.__check_and_send(data, vehicle=vehicle)

    def send_rosoutagg(
        self,
        type_msg: str = None,
        msg: str = None,
        vehicle: str = None,
        time_stamp: str = None,
        file: str = None,
        function: str = None,
        line: int = None,
    ):
        """
        Send information and warnings about the robot
        """
        data = {
            "event": "rosout_agg",
            "type": type_msg,
            "msg": msg,
            "vehicle": vehicle,
            "time stamp": time_stamp,
            "file": file,
            "function": function,
            "line": line,
        }

    def send_offlinelog(self):
        tasks_log_file_path = join(expanduser("~"), ".meili/log_tasks.csv")
        json_file_path = join(expanduser("~"), ".meili/log_tasks.json")

        if exists(tasks_log_file_path) and isfile(tasks_log_file_path):
            data = {}
            with open(tasks_log_file_path, mode="r") as csv_file:
                csv_reader = csv.DictReader(csv_file)
                line_count = 0
                for rows in csv_reader:
                    key = rows["no"]
                    data[key] = {"event": "offlinetask"}
                    data[key].update(rows)
            with open(json_file_path, "w", encoding="utf-8") as json_file:
                json_file.write(json.dumps(data, indent=4))

            try:
                headers = {
                    "Content-Type": "application/json",
                    "X-Meili-Setup-Token": self.setup_token,
                }

                url_offline = (
                    "https://"
                    + str((self.server_instance.value))
                    + ".meilirobots.com/api/tasks/offline/sync/"
                )

                r = requests.post(url_offline, data=json.dumps(data), headers=headers, timeout=10)
            except Exception as e:
                capture_exception(e)

            ts = round(datetime.timestamp(datetime.now()))
            new_fpath = join(expanduser("~"), ".meili/log_tasks_" + str(ts) + ".csv")
            os.rename(tasks_log_file_path, new_fpath)

            return True
        else:
            return False

    def send_gps_data(self, vehicle_token: str, lat, lon, rotation):
        """
        Send metric location data to the server
        """

        data = {
            "event": "gps_location",
            "vehicle": vehicle_token,
            "value": {"lat": lat, "lon": lon, "rotation": rotation},
        }
        return self.__connection.send(json.dumps(data))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
