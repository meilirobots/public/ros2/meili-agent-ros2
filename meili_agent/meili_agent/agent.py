import datetime
from os.path import join
from functools import partial

import numpy

# Meili agent dependencies
from meili_ros_lib.sentry import initialize_sentry
from meili_ros_lib.agent import Agent
from meili_ros_lib.maths import euler_to_quaternion

# ROS2 dependencies
from ament_index_python.packages import get_package_share_directory
from ros2topic.api import get_msg_class

from meili_agent_msgs.msg import Topicinit
from nav2_msgs.action import FollowWaypoints, NavigateToPose
from rcl_interfaces.msg import Parameter, ParameterType, ParameterValue

from geometry_msgs.msg import PoseWithCovarianceStamped, Quaternion
from std_msgs.msg import Header


initialize_sentry()


class MeiliAgent(Agent):
    """
    Class for interact with ROS and Meili Cloud.
    """

    def __init__(self, node):
        package_name = "meili_agent"
        package_path = get_package_share_directory(package_name)
        file_name_topics = join(package_path, "defaults.json")
        file_name_vehicles = join(package_path, "config.yaml")

        Agent.__init__(self, node, file_name_vehicles, file_name_topics)

        # ROS2
        self.req_set = []
        self.req_get = []
        self.cli_set = []
        self.cli_get = []
        self.created_log_file = False

        # ROS2 Goal
        self.client_list = []
        self.client_waypoints_list = None

        self.goal_handle_waypoints = []
        self.goal_handle = []

        self.current_goal = []
        self.total_waypoints = None
        self.goal_id_waypoints = None
        self.waypoint_position = []

        for i in range(0, self.number_of_vehicles):
            self.current_goal.append(None)
            self.goal_handle.append(None)
            self.goal_handle_waypoints.append(None)
            self.waypoint_position.append(None)


    def callback_rosoutagg_ros2(self, msg):
        bytes = [10, 20, 30, 40, 50]  # ROS2
        levels = numpy.array(['debug', 'info', 'warning', 'error', 'fatal'])
        type_msg = levels[bytes.index(msg.level)]

        self.callback_rosoutagg(msg, type_msg)

    @staticmethod
    def status_ros_to_ros2(status_id):
        return {4: 3, 2: 1, 6: 4}.get(status_id, status_id)

    # CALLBACKS
    def check_waypoint_update(self, vehicle_position):
        if self.waypoints[vehicle_position] and (
                self.waypoint_position[0] + 1 == self.total_waypoints
        ):
            self.waypoints[vehicle_position] = False

    def callback_status_ros2(self, msg, vehicle_uuid):
        # ROS2
        if msg.status_list and len(msg.status_list) != 0:
            last_status = msg.status_list[len(msg.status_list) - 1]
            goal_id = str(numpy.ndarray.tolist(last_status.goal_info.goal_id.uuid))  # ROS2
            status_id = self.status_ros_to_ros2(last_status.status)  # ROS2

            # ROS2 : Check if it's a waypoint and update the variable of waypoints when it is done
            vehicle_position = self.return_vehicle_position(vehicle_uuid)
            if (status_id in (3, 4, 5)) and self.task_started[vehicle_position] == 1:
                self.check_waypoint_update(vehicle_position)

            self.callback_status(msg, goal_id, status_id, vehicle_uuid)

    # ROS2
    def callback_status_waypoints(self, msg):
        """Status of waypoints navigation task is parsed and send to FMS agent"""
        status_id = None
        if msg.status_list and len(msg.status_list) != 0:
            last_status = msg.status_list[len(msg.status_list) - 1]
            status_id = self.status_ros_to_ros2(last_status.status)

        # IF EXECUTING, SUCCEEDED, ABORTED, CANCELED

        if self.connection.offline_agent.task_offline_started and status_id == 3:
            success = True
            self.connection.offline_agent.task_offline_started = False
            self.created_log_file = True
            self.start_time = None

            for task in self.connection.offline_agent.offline_tasks:
                if self.connection.offline_agent.current_task_uuid == task[0]:
                    self.connection.offline_agent.logging_offlinetasks(
                        task,
                        self.number_of_tasks,
                        self.vehicle_list,
                        self.start_time,
                        success=success,
                    )
                    self.connection.offline_agent.current_task_uuid = None

        elif status_id == 1 and self.start_time:
            now = datetime.datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            self.start_time = dt_string

    ################################################
    # ROS2
    def send_change_speed_request(self, max_vel_x, max_vel_theta, vehicle_position):
        try:
            new_max_vel_x_value = ParameterValue(
                type=ParameterType.PARAMETER_DOUBLE, double_value=float(max_vel_x)
            )
            new_max_vel_theta_value = ParameterValue(
                type=ParameterType.PARAMETER_DOUBLE, double_value=float(max_vel_theta)
            )
            self.req_get[vehicle_position].names = ["FollowPath.max_vel_x", "FollowPath.max_vel_theta"]
            self.req_set[vehicle_position].parameters = [
                Parameter(name="FollowPath.max_vel_x", value=new_max_vel_x_value),
                Parameter(name="FollowPath.max_vel_theta", value=new_max_vel_theta_value),
            ]
            self.cli_get[vehicle_position].wait_for_service()

            future_get = self.cli_get[vehicle_position].call_async(self.req_get[vehicle_position])
            future_get.add_done_callback(
                partial(self.callback_get_global_param, vehicle_position=vehicle_position)
            )

            future_set = self.cli_set[vehicle_position].call_async(self.req_set[vehicle_position])
            future_set.add_done_callback(
                partial(self.callback_set_global_param, vehicle_position=vehicle_position)
            )
                    
            self.log_info(
                f"[ROSAgent] Max_vel_x changed to: {max_vel_x} and max_vel_theta changed to: {max_vel_theta}"
            )
        except Exception as e:
            self.log_error(f"[ROSAgent] Send Change Speed Request Error:{e}")

    # ROS2
    def callback_get_global_param(self, future, vehicle_position):
        try:
            result = future.result()

        except Exception as e:
            self.node.get_logger().warn(f"[ROSAgent] service call failed {e}")
        else:
            self.max_vel_x[vehicle_position] = result.values[0].double_value
            self.max_vel_theta[vehicle_position] = result.values[0].double_value
            self.log_info(
                f"[ROSAgent] Initial max_vel_x was: {self.max_vel_x[vehicle_position]} "
                f"and initial max_vel_theta was {self.max_vel_theta[vehicle_position]} "
            )
            
    def callback_set_global_param(self, future, vehicle_position):
        try:
            future.result()
        except Exception as e:
            self.node.get_logger().warn(
                f"[ROSAgent] Failed to set parameters for vehicle {vehicle_position}. Error: {e}")
        else:
            self.log_info(
                f"[ROSAgent] Parameters successfully set for vehicle {vehicle_position}."
            )

    ################# GOAL ROS2 ####################

    def feedback_callback(self, feedback_msg: NavigateToPose.Feedback):
        """NavigateToPose should have no feedback"""
        # self.goal_id = feedback_msg.goal_id

    def goal_response_callback(self, future, vehicle_position):
        """Goal Response"""
        self.goal_handle[vehicle_position] = future.result()
        if not self.goal_handle[vehicle_position].accepted:
            self.node.get_logger().warning("[ROSHandler] Goal rejected :(")
            return

        self.log_info("[ROSHandler] Goal accepted :)")
        self.node._get_result_future = self.goal_handle[vehicle_position].get_result_async()
        self.node._get_result_future.add_done_callback(self.get_result_callback)

    @staticmethod
    def get_result_callback(future: NavigateToPose.Result):
        result = future.result().result
        return result

    ############ GOAL_WAYPOINTS ROS2 ###############

    def feedback_callback_waypoints(self, feedback_msg: FollowWaypoints.Feedback):
        """NavigateToPose should have no feedback"""
        waypoint_position = feedback_msg.feedback.current_waypoint
        # TODO: to be able to get the waypoint_position and total_waypoint for multiple robots
        # maybe comparing the goal:id with the robot assigned vehicle_position = self.return_vehicle_position(vehicle)
        self.node.get_logger().debug(f"{feedback_msg}")
        if waypoint_position is not self.waypoint_position[0]:
            self.log_info(
                f"[ROSAgent] Executing {waypoint_position + 1}/{self.total_waypoints} waypoint"
            )
        self.goal_id_waypoints = feedback_msg.goal_id
        self.node.get_logger().debug("[ROSAgent] Received FollowWaypoints feedback")
        self.waypoint_position[0] = waypoint_position

    def goal_response_callback_waypoints(self, future, vehicle_position):
        self.goal_handle_waypoints[vehicle_position] = future.result()
        if not self.goal_handle_waypoints[vehicle_position].accepted:
            self.node.get_logger().warning("[ROSAgent] Goal  waypoints rejected :(")
            return
        self.log_info("[ROSAgent] Goal accepted waypoints:)")
        self.node._get_result_future = self.goal_handle_waypoints[vehicle_position].get_result_async()
        self.node._get_result_future.add_done_callback(
            self.get_result_callback_waypoints
        )

    @staticmethod
    def get_result_callback_waypoints(future: FollowWaypoints.Result):
        result_waypoints = future.result().result
        return result_waypoints

    ################################################

    def topic_streaming_talker(self, msg):
        # ROS2
        """Publish Ros Message to topic streaming node if there is topics to stream"""
        pub = self.node.create_publisher(Topicinit, "/topic_init", qos_profile=0)
        rate = self.node.create_rate(1)
        # wait for having the topic streaming node subscribe
        while pub.get_num_connections() < 1:
            pass
        pub.publish(msg)
        rate.sleep()

    ################################################
    
    def remove_vehicle_from_fleet(self, vehicle):
        """Remove vehicle from fleet"""
        
        # Stop agent node from spinning
        self.node.destroy_node()
        self.node.get_logger().info("[ROSAgent] Node destroyed")
        self.node.get_logger().info("[ROSAgent] Agent stopped")
        self.node.get_logger().info("[ROSAgent] Exiting")

        self.vehicle_list.remove(vehicle)
        self.number_of_vehicles = len(self.vehicle_list)
        self.log_info(f"[ROSAgent] Vehicle {vehicle} removed from fleet")
        self.log_info(f"[ROSAgent] Number of vehicles in fleet: {self.number_of_vehicles}")
        self.log_info(f"[ROSAgent] Fleet needs to be set up again with a new pincode")

    def set_initial_pose(self, vehicle_position, x, y, yaw):
        """Set estimated pose of vehicle"""
        self.log_info(f"[ROSAgent] Setting estimated pose for vehicle with token: " 
                      f"{self.vehicles[vehicle_position]['token']} to x={x}, y={y}, yaw={yaw}")
        try:
            # Get vehicle initialpose topic
            vehicle = self.vehicles[vehicle_position] 
            topic = vehicle["topics"]["initialpose"]["topic"]

            # Create message
            initial_pose_class = get_msg_class(
                self.node,
                topic,
                include_hidden_topics=True,
            )

            if initial_pose_class == type(PoseWithCovarianceStamped()):
                # Make an instance of initial pose
                initial_pose = PoseWithCovarianceStamped()
                initial_pose.header = Header()
                
                # Current time
                initial_pose.header.stamp = self.node.get_clock().now().to_msg()
                
                # The reference frame, typically "map"
                initial_pose.header.frame_id = "map"

                initial_pose.pose.pose.position.x = float(x)
                initial_pose.pose.pose.position.y = float(y)
                initial_pose.pose.pose.position.z = 0.0
                
                quaternion = Quaternion()
                quaternions_conv = euler_to_quaternion(0.0, 0.0, float(yaw))
                quaternion.x = quaternions_conv["qx"]
                quaternion.y = quaternions_conv["qy"]
                quaternion.z = quaternions_conv["qz"]
                quaternion.w = quaternions_conv["qw"]
                initial_pose.pose.pose.orientation = quaternion
                
                # Low uncertainty covariance, make our new estimation as a priority.
                initial_pose.pose.covariance = [0.1] * 36  
                
                pose_publisher = self.node.create_publisher(PoseWithCovarianceStamped, topic, 10)
                pose_publisher.publish(initial_pose)
                
                # Kill the publisher as this functionality will be used rather infrequently.
                self.node.destroy_publisher(pose_publisher)
            else:
                self.log_info(f"[ROSHandler] Only PoseWithCovarianceStamped() is supported as a message "
                              f"type to estimate vehicle pose. Other types need custom implementation.")
        except Exception as e:
            self.log_error(f"[ROSAgent] Send Initial Position Request Error:{e}")
