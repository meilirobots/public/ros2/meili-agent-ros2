from functools import partial

from rclpy.qos import qos_profile_sensor_data
from ros2topic.api import get_msg_class


class PubSub:
    def __init__(self, node, agent):
        self.node = node
        self.agent = agent

    def battery_sub(self, vehicle):
        """Initiating Battery Subscriber"""
        message_type_battery = get_msg_class(
            self.node,
            vehicle["topics"]["battery"]["topic"],
            include_hidden_topics=True,
        )

        self.node.create_subscription(
            message_type_battery,
            vehicle["topics"]["battery"]["topic"],
            partial(
                self.agent.callback_battery,
                vehicle_uuid=vehicle["uuid"],
            ),
            qos_profile=qos_profile_sensor_data,
        )

    def gps_sub(self, vehicle):
        message_type_gps = get_msg_class(
            self.node,
            vehicle["topics"]["gps"]["topic"],
            include_hidden_topics=True,
        )
        self.node.create_subscription(
            message_type_gps,
            vehicle["topics"]["battery"]["topic"],
            partial(
                self.agent.callback_gps,
                vehicle_uuid=vehicle["uuid"],
            ),
            qos_profile=qos_profile_sensor_data,
        )

    def pose_sub(self, vehicle):
        message_type_pose = get_msg_class(
            self.node,
            vehicle["topics"]["pose"]["topic"],
            include_hidden_topics=True,
        )
        self.node.create_subscription(
            message_type_pose,
            vehicle["topics"]["pose"]["topic"],
            partial(self.agent.callback_pose, vehicle_uuid=vehicle["uuid"]),
            qos_profile=qos_profile_sensor_data,
        )

    def status_sub(self, vehicle):
        message_type_goal_status = get_msg_class(
            self.node,
            vehicle["topics"]["goalArray"]["topic"],
            include_hidden_topics=True,
        )
        self.node.create_subscription(
            message_type_goal_status,
            vehicle["topics"]["goalArray"]["topic"],
            partial(self.agent.callback_status_ros2, vehicle_uuid=vehicle["uuid"]),
            qos_profile=0,
        )

    def goal_pub(self, vehicle):
        message_type_goal = get_msg_class(
            self.node,
            vehicle["topics"]["goal"]["topic"],
            include_hidden_topics=True,
        )
        self.node.create_publisher(
            message_type_goal,
            vehicle["topics"]["goal"]["topic"],
            qos_profile=qos_profile_sensor_data,
        )
        # TODO goal_canceller

    def plan_sub(self, vehicle):
        message_type_plan = get_msg_class(
            self.node,
            vehicle["topics"]["plan"]["topic"],
            include_hidden_topics=True,
        )

        self.node.create_subscription(
            message_type_plan,
            vehicle["topics"]["plan"]["topic"],
            partial(self.agent.callback_plan, vehicle_uuid=vehicle["uuid"]),
            qos_profile=0,
        )

    def speed_sub(self, vehicle):
        message_type_speed = get_msg_class(
            self.node,
            vehicle["topics"]["speed"]["topic"],
            include_hidden_topics=True
        )
        self.node.create_subscription(
            message_type_speed,
            vehicle["topics"]["speed"]["topic"],
            partial(self.agent.callback_speed, vehicle_uuid=vehicle["uuid"]),
            qos_profile=0,

        )

    def rosout_sub(self):
        message_type_rosoutagg = get_msg_class(
            self.node, "rosout", include_hidden_topics=True
        )

        self.node.create_subscription(
            message_type_rosoutagg,
            "rosout",
            self.agent.callback_rosoutagg_ros2,
            qos_profile=0,
        )

    # ROS2
    def waypoints_sub(self, vehicle):
        """Initiating Waypoints Subscriber"""

        message_type_goal_status = get_msg_class(
            self.node,
            vehicle["topics"]["goalArray"]["topic"],
            include_hidden_topics=True,
        )
        self.node.create_subscription(
            message_type_goal_status,
            vehicle["topics"]["waypointsGoalArray"]["topic"],
            self.agent.callback_status_waypoints,
            qos_profile=0,
        )


def pub_and_sub(node, agent):
    """Creating Subscriber and Publisher for ROS2 Nodes"""
    pubsub = PubSub(node, agent)
    node.get_logger().info("[PubSub] Starting Subscription and Publishing of Topics")

    try:
        for vehicle in agent.vehicles:

            pubsub.pose_sub(vehicle)
            pubsub.status_sub(vehicle)
            pubsub.rosout_sub()

            pubsub.goal_pub(vehicle)
            pubsub.speed_sub(vehicle)

            if node.battery_present:
                pubsub.battery_sub(vehicle)

            if node.outdoor:
                pubsub.gps_sub(vehicle)

            if node.traffic_control:
                node.get_logger().info("[PubSub] Traffic Control Activated")
                pubsub.plan_sub(vehicle)

            # ROS2
            if node.path_planning:
                pubsub.waypoints_sub(vehicle)

            name = vehicle["prefix"]
            node.get_logger().info(
                f"[PubSub] Completed publishing and subscription of topics for vehicle: {name}"
            )

    except Exception as error:
        node.get_logger().error(
            f"[PubSub] Publishing and subscription error: {error}"
        )
