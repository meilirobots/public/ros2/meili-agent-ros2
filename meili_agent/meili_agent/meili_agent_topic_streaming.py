#!/usr/bin/env python3
import sys
import threading
from functools import partial
from json import dumps, loads
from os.path import join

import influxdb_client
import rclpy
import yaml
from ament_index_python.packages import get_package_share_directory
from influxdb_client.client.write_api import SYNCHRONOUS
from meili_agent.mqtt_client import MQTTClient
from meili_agent_msgs.msg import Topicinit
from rclpy.node import Node
from ros2topic.api import get_msg_class
from rosidl_runtime_py import message_to_ordereddict
from sentry_sdk import capture_exception, capture_message

frequency = None


class create_node(Node):
    """
    Class for creating ROS2 node and inputs parameters
    """

    def __init__(self):
        super().__init__("topic_streaming")
        self.declare_parameter("publish_frequency")
        self.declare_parameter("mqtt_id")
        self.declare_parameter("influx_bucket")
        self.declare_parameter("influx_org")
        self.declare_parameter("influx_token")
        self.declare_parameter("influx_url")
        global frequency, influx_bucket, influx_org, influx_token, influx_url
        frequency = (
            self.get_parameter("publish_frequency").get_parameter_value().integer_value
        )
        influx_bucket = (
            self.get_parameter("influx_bucket").get_parameter_value().string_value
        )
        influx_org = self.get_parameter("influx_org").get_parameter_value().string_value
        influx_token = (
            self.get_parameter("influx_token").get_parameter_value().string_value
        )
        influx_url = self.get_parameter("influx_url").get_parameter_value().string_value


class Topic_msg_server:
    """
    Class to subscribe, store and send ROS topics to Meili Cloud using InfluxDB connector or MQT
    """

    def __init__(self):
        self.node = None
        self.subscriber = None
        self.msg = []
        self.topic_uuid = []
        self.message_type = []
        self.vehicle_token = []
        self.topic = []
        self.mqtt = None
        self.block = False
        self.message_class = None
        self.mqtt_id = None
        self.publish_frequency = None
        self.subscription = []
        self.number_of_topics = 0

        self.topic_streaming_method = "influx"

        self.influx_bucket = None
        self.influx_org = None
        self.influx_token = None
        self.influx_url = None
        self.influx_write_api = None
        self.influx_client = None

        self.total_count = 0
        self.number_of_vehicles = 0
        self.vehicle_order = []
        self.prefix = []

    def callback(self, msg):
        self.msg.append("1")
        self.vehicle_token.append(msg.vehicle_token)
        self.topic_uuid.append(msg.topic_uuid)
        self.topic.append(msg.topic_name)
        self.message_type.append(msg.topic_type)
        self.publish_frequency = msg.frequency
        self.mqtt_id = msg.mqtt_id
        self.number_of_topics = self.number_of_topics + 1
        self.prefix.append(msg.prefix)

        topic_info = {}
        topic_info["topic"] = msg.topic_name
        topic_info["topic_uuid"] = msg.topic_uuid
        topic_info["vehicle_token"] = msg.vehicle_token
        topic_info["topic_type"] = msg.topic_type
        topic_info["order"] = self.number_of_topics

        topic_info["topic_order"] = int(msg.total_count)
        topic_info["vehicle_order"] = int(msg.number_of_vehicles)
        self.total_count = int(msg.total_count)
        self.number_of_vehicles = topic_info["vehicle_order"]
        w, h = self.total_count, self.number_of_vehicles
        self.msg = [[0 for x in range(w)] for y in range(h)]
        try:
            self.message_class = get_msg_class(
                self.node, topic_info["topic"], include_hidden_topics=True
            )
        except Exception as KeyError:
            self.node.get_logger().info(
                "[ROSAgent_TOPIC_STREAMING] Error reading msg class"
            )
            capture_exception(KeyError)
            capture_message("[ROSAgent_TOPIC_STREAMING] Error reading msg class")

        self.node.get_logger().info(
            "[ROSAgent_TOPIC_STREAMING] topic name is: %s" % (msg.topic_name,)
        )
        self.subscription.append(
            self.node.create_subscription(
                self.message_class,
                topic_info["topic"],
                partial(self.topic_callback, topic_info=topic_info),
                1,
            )
        )

    def topic_callback(self, msg, topic_info):
        msg_converted = self.to_dict(message_to_ordereddict(msg))
        
        for _ in range(0, self.number_of_vehicles):
            for __ in range(0, self.total_count):
                self.msg[topic_info["vehicle_order"] - 1][
                    topic_info["topic_order"] - 1
                ] = msg_converted

    def run(self):
        # msg will be sent to the server to the predefined freauency
        rate = self.node.create_rate(self.publish_frequency)
        rate.sleep()
        self.node.get_logger().info(
            f"[ROSAgent_TOPIC_STREAMING] Topic streaming frequency {self.publish_frequency}"
        )
        while True:
            count = 0
            rate.sleep()
            for index in range(0, self.number_of_vehicles):
                if self.topic_streaming_method == "mqtt":
                    self.mqtt.send_topic_data(
                        self.topic_uuid[count],
                        self.msg[count],
                        self.vehicle_token[count],
                        self.message_type[count],
                        self.topic[count],
                    )
                    count = count + 1
                elif self.topic_streaming_method == "influx":
                    for index2 in range(0, self.total_count):
                        if self.prefix[index2] != 0:
                            topic_odom = self.prefix[index2] + "/odom"
                            topic_imu = self.prefix[index2] + "/imu"
                            topic_amcl = self.prefix[index2] + "/amcl_pose"
                            topic_vel = self.prefix[index2] + "/cmd_vel"
                            if self.topic[index2] == topic_odom:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[
                                                    index
                                                ],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "xpos": self.msg[index][index2][
                                                    "pose"
                                                ]["pose"]["position"]["x"],
                                                "ypos": self.msg[index][index2][
                                                    "pose"
                                                ]["pose"]["position"]["y"],
                                                "zpos": self.msg[index][index2][
                                                    "pose"
                                                ]["pose"]["position"]["z"],
                                            },
                                        }
                                    ]

                                    self.influx_write_api.write(
                                        self.influx_bucket, self.influx_org, data
                                    )
                            if self.topic[index2] == topic_amcl:

                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[
                                                    index
                                                ],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "xpos": self.msg[index][index2][
                                                    "pose"
                                                ]["pose"]["position"]["x"],
                                                "ypos": self.msg[index][index2][
                                                    "pose"
                                                ]["pose"]["position"]["y"],
                                                "zpos": self.msg[index][index2][
                                                    "pose"
                                                ]["pose"]["position"]["z"],
                                            },
                                        }
                                    ]

                            elif self.topic[index2] == topic_imu:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[
                                                    index
                                                ],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "x.orientation": self.msg[index][
                                                    index2
                                                ]["orientation"]["x"],
                                                "y.orientation": self.msg[index][
                                                    index2
                                                ]["orientation"]["y"],
                                                "z.orientation": self.msg[index][
                                                    index2
                                                ]["orientation"]["z"],
                                                "x.angular_vel": self.msg[index][
                                                    index2
                                                ]["angular_velocity"]["x"],
                                                "y.angular_vel": self.msg[index][
                                                    index2
                                                ]["angular_velocity"]["y"],
                                                "z.angular_vel": self.msg[index][
                                                    index2
                                                ]["angular_velocity"]["z"],
                                                "x.linear_acce": self.msg[index][
                                                    index2
                                                ]["linear_acceleration"]["x"],
                                                "y.linear_acce": self.msg[index][
                                                    index2
                                                ]["linear_acceleration"]["y"],
                                                "z.linear_acce": self.msg[index][
                                                    index2
                                                ]["linear_acceleration"]["z"],
                                            },
                                        }
                                    ]
                                self.influx_write_api.write(
                                    self.influx_bucket, self.influx_org, data
                                )
                            elif self.topic[index2] == topic_vel:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[
                                                    index
                                                ],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "x.linear": self.msg[index][index2][
                                                    "linear"
                                                ]["x"],
                                                "y.linear": self.msg[index][index2][
                                                    "linear"
                                                ]["y"],
                                                "z.linear": self.msg[index][index2][
                                                    "linear"
                                                ]["z"],
                                                "x.angular": self.msg[index][
                                                    index2
                                                ]["angular"]["x"],
                                                "y.angular": self.msg[index][
                                                    index2
                                                ]["angular"]["y"],
                                                "z.angular": self.msg[index][
                                                    index2
                                                ]["angular"]["z"],
                                            },
                                        }
                                    ]

                                    self.influx_write_api.write(
                                        self.influx_bucket, self.influx_org, data
                                    )
                            else:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[
                                                    index
                                                ],
                                            },
                                            "fields": {
                                                "topic_uuid": str(
                                                    self.topic_uuid[index2]
                                                ),
                                                "msg": str(self.msg[index][index2]),
                                            },
                                        }
                                    ]


    def config_agent(self):
        # config agent by reading and storing config files about number of vehicles, prefixes and topics
        package_name = "meili_agent"
        package_path = get_package_share_directory(package_name)
        filenamevehicles = join(package_path, "config.yaml")
        data = None
        # Parse Mode, number of vehicles and tokens from config files passed as arguments
        with open(filenamevehicles, "r") as config:
            data = yaml.safe_load(config)

        if data is None:
            self.node.get_logger().info("[ROSAgent_TOPIC_STREAMING] in exit")
            sys.exit()
        try:
            if (self.topic_streaming_method) != "influx":
                self.mqtt_id = data["mqttid"]
            self.publish_frequency = frequency
            self.influx_bucket = influx_bucket
            self.influx_org = influx_org
            self.influx_token = influx_token
            self.influx_url = influx_url
        except KeyError as e:
            self.node.get_logger().warning(f"KeyError: {e}")
        except Exception as e:
            capture_exception(e)

    def to_dict(self, input_ordered_dict):
        return loads(dumps(input_ordered_dict))


def main(args=None):
    rclpy.init(args=args)
    topic_msg_server = Topic_msg_server()
    topic_msg_server.node = create_node()
    topic_msg_server.config_agent()
    rate = topic_msg_server.node.create_rate(2)
    topic_msg_server.subscriber = topic_msg_server.node.create_subscription(
        Topicinit, "/topic_init", topic_msg_server.callback, qos_profile=0
    )

    if (topic_msg_server.topic_streaming_method) != "influx":
        if topic_msg_server.mqtt_id == None:
            topic_msg_server.node.get_logger().info(
                "[ROSAgent_TOPIC_STREAMING] /mqtt_id parameter is not set, check your config file, closing streaming topics node"
            )
            sys.exit(1)

    if topic_msg_server.publish_frequency == None:
        topic_msg_server.publish_frequency = 1
        topic_msg_server.node.get_logger().info(
            "[ROSAgent_TOPIC_STREAMING] publish frequency is not set up, using 1hz"
        )
    else:
        topic_msg_server.node.get_logger().info(
            "[ROSAgent_TOPIC_STREAMING] Publish frequency for topic streaming is: %f"
            % (topic_msg_server.publish_frequency,)
        )
    if topic_msg_server.topic_streaming_method == "mqtt":
        try:
            topic_msg_server.node.get_logger().info(
                "[ROSAgent_TOPIC_STREAMING] Mqttid is: %s" % (topic_msg_server.mqtt_id,)
            )
            topic_msg_server.mqtt = MQTTClient(client_id=topic_msg_server.mqtt_id)
            topic_msg_server.mqtt.start()
            thread = threading.Thread(
                target=rclpy.spin, args=(topic_msg_server.node,), daemon=True
            )
            thread.start()
            topic_msg_server.run()

        except Exception as e:
            capture_exception(e)
    elif topic_msg_server.topic_streaming_method == "influx":
        thread = threading.Thread(
            target=rclpy.spin, args=(topic_msg_server.node,), daemon=True
        )
        thread.start()
        topic_msg_server.influx_client = influxdb_client.InfluxDBClient(
            url=topic_msg_server.influx_url, token=topic_msg_server.influx_token
        )
        topic_msg_server.influx_write_api = topic_msg_server.influx_client.write_api(
            write_options=SYNCHRONOUS
        )
        topic_msg_server.run()

    topic_msg_server.node.destroy_node()
    rclpy.shutdown()
