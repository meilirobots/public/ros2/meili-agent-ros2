# parse_data.py
import numpy
import math
import typing as t

from action_msgs.msg import GoalInfo
from geometry_msgs.msg import PoseStamped, Twist
from nav2_msgs.action import FollowWaypoints, NavigateToPose
from sentry_sdk import capture_exception

from meili_agent_msgs.msg import Topicinit
from meili_ros_lib.maths import quaternion_to_euler, euler_to_quaternion
from meili_ros_lib.agent import agent_sentry


def goal_waypoints_setup(pose, node) -> t.List:
    """Collecting all the waypoints
    Returns: List of Pose information"""
    try:

        x_meters = [item[0] for item in pose]
        y_meters = [item[1] for item in pose]

        try:
            rotation = [item[2] for item in pose]
        except IndexError:
            rotation = [0 for _ in pose]

        rotation_converted = []
        for i in range(len(x_meters)):
            rotation_converted.append(
                euler_to_quaternion(0, 0, math.radians(float(rotation[i])))
            )
        poses = parse_goal_waypoints(
            x_meters, y_meters, rotation_converted, node
        )
    except TypeError as error:
        agent_sentry(error)
        raise
    return poses


def parse_goal(x, y, rotation_quaternion, node):
    try:
        xconv = float(x)
        goal = NavigateToPose.Goal()
        goal.pose.header.stamp = node.get_clock().now().to_msg()
        goal.pose.header.frame_id = "map"
        goal.pose.pose.position.x = xconv
        goal.pose.pose.position.y = float(y)
        goal.pose.pose.position.z = 0.0
        goal.pose.pose.orientation.x = rotation_quaternion["qx"]
        goal.pose.pose.orientation.y = float(rotation_quaternion["qy"])
        goal.pose.pose.orientation.z = float(rotation_quaternion["qz"])
        goal.pose.pose.orientation.w = float(rotation_quaternion["qw"])
    except ValueError as e:
        capture_exception(e)
        raise
    return goal


def parse_goal_waypoints(x, y, rotation_quaternion, node):
    try:
        xconv = numpy.array(x, float)
        yconv = numpy.array(y, float)
        number_waypoints = len(x)
        poses = []

        if number_waypoints != len(yconv) or number_waypoints != len(
                rotation_quaternion
        ):
            raise IndexError

        for i in range(number_waypoints):
            pose = PoseStamped()
            pose.header.stamp = node.get_clock().now().to_msg()
            pose.header.frame_id = "map"
            pose.pose.position.x = xconv[i]
            pose.pose.position.y = yconv[i]
            pose.pose.position.z = 0.0
            pose.pose.orientation.x = float(rotation_quaternion[i]["qx"])
            pose.pose.orientation.y = float(rotation_quaternion[i]["qy"])
            pose.pose.orientation.z = float(rotation_quaternion[i]["qz"])
            pose.pose.orientation.w = float(rotation_quaternion[i]["qw"])
            poses.append(pose)

        goal_msg = FollowWaypoints.Goal()
        goal_msg.poses = poses
    except (ValueError, IndexError) as e:
        capture_exception(e)
        raise
    return goal_msg


def parse_cancel_goal(goal_id):
    goal = GoalInfo()
    return goal


def parse_vel(max_vel_x):
    # TO DO: get the current parameters and just change the linear.x
    vel = Twist()

    vel.linear.x = 0.0  # float(max_vel_x)
    vel.linear.y = 0.0
    vel.linear.z = 0.0

    vel.angular.x = 0.0
    vel.angular.y = 0.0
    vel.angular.z = 0.0

    return vel


def parse_topic(data, agent):
    vehicle_position = agent.return_vehicle_position(data["vehicle_uuid"])

    msg = Topicinit()
    msg.vehicle_token = data["vehicle_uuid"]
    msg.topic_uuid = data["uuid"]
    msg.topic_name = data["topic"]
    msg.topic_type = data["message_type"]

    msg.mqtt_id = agent.mqtt_id
    msg.frequency = int(agent.publish_frequency)

    msg.total_count = agent.total_topics
    msg.number_of_vehicles = vehicle_position + 1
    msg.prefix = agent.vehicles[vehicle_position]["prefix"]

    return msg


def parse_docking_routine(result):
    path = [[pose.pose.position.x, pose.pose.position.y] for pose in result.poses]
    rotation_angles = [[round(quaternion_to_euler(
        pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z, pose.pose.orientation.w
    ), 3)] for pose in result.poses]

    return path, rotation_angles
