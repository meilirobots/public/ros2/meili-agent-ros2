"""Handles the Websocket information"""

# ROS2 dependencies
from functools import partial

import rclpy
from rclpy.action import ActionClient
from std_msgs.msg import Bool, String
from nav2_msgs.srv import LoadMap

# Meili agent dependencies
from action_docking.action import Docking
from meili_agent.parse_data import parse_goal, parse_goal_waypoints, parse_topic, goal_waypoints_setup

# Meili Lib dependencies
from meili_ros_lib.sdk_handlers import Handlers
from meili_ros_lib.sentry import initialize_sentry
from meili_ros_lib.parse_data import goal_setup, rotation_setup, parse_vel

import requests
import yaml
import os


initialize_sentry()

def prepare_map_files(png_url, yaml_metadata):
    username = os.getlogin()
    download_dir = "/home/"+str(username)+"/Downloads"

    # Define file paths
    image_filename = "meili_map.png"
    image_path = os.path.join(download_dir, image_filename)
    yaml_filename = image_filename.replace('.png', '.yaml')
    yaml_path = os.path.join(download_dir, yaml_filename)

    # Step 1: Download the PNG image
    response = requests.get(png_url, timeout=10)
    if response.status_code == 200:
        with open(image_path, 'wb') as img_file:
            img_file.write(response.content)
        print(f"Downloaded image to {image_path}")
    else:
        raise Exception(f"Failed to download image from {png_url}")

    # Step 2: Create the YAML file
    yaml_content = {
        'image': image_filename,
        'resolution': float(yaml_metadata['resolution']),
        'origin': [float(val) for val in yaml_metadata['origin']],
        'negate': yaml_metadata['negate'],
        'occupied_thresh': float(yaml_metadata['occupied_thresh']),
        'free_thresh': float(yaml_metadata['free_thresh']) if yaml_metadata['free_thresh'] is not None else 0.196,
        'mode': yaml_metadata['mode']
    }

    # Write YAML content to file
    with open(yaml_path, 'w') as yaml_file:
        yaml.dump(yaml_content, yaml_file, default_flow_style=False)
    
    print(f"YAML file created at {yaml_path}")
    
    # Step 3: Return the path to the YAML file
    return yaml_path



class SDKHandlers(Handlers):
    """Class for all the handlers of messages coming from SDK"""

    def __init__(self, agent):
        Handlers.__init__(self, agent)
        self.rate = self.agent.node.create_rate(1)

    def send_goal(self, data, vehicle_position):
        """Sends Goal information to specific vehicle"""
        try:
            x_meters, y_meters, rotation_quaternion = goal_setup(data)
            goal = parse_goal(x_meters, y_meters, rotation_quaternion, self.agent.node)

            if self.agent.task_started[vehicle_position] == 0:
                # ROS2
                self.agent.node._send_goal_future = self.agent.client_list[
                    vehicle_position
                ].send_goal_async(
                    goal, feedback_callback=self.agent.feedback_callback
                )
                self.agent.node._send_goal_future.add_done_callback(partial(self.agent.goal_response_callback,
                                                                            vehicle_position=vehicle_position))
        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Send Goal Error >> {e}")

    def send_waypoints(self, pose, vehicle_position):
        try:
            x_meters = [item[0] for item in pose]

            # ROS2
            rate = self.agent.node.create_rate(1)
            self.agent.total_waypoints = len(x_meters)
            poses = goal_waypoints_setup(pose, self.agent.node)

            self.agent.node._send_goal_future = self.agent.client_waypoints_list[
                vehicle_position
            ].send_goal_async(poses, feedback_callback=self.agent.feedback_callback_waypoints)

            self.agent.node._send_goal_future.add_done_callback(
                partial(self.agent.goal_response_callback_waypoints,
                        vehicle_position=vehicle_position)
            )
            rate.sleep()

            self.agent.log_info(
                f"[ROSHandler] Received waypoints task assigned to vehicle: {self.agent.vehicle_list[vehicle_position]}"
            )
        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Send Waypoints error >> {e}")

    def cancel_goal(self, _, vehicle_position):
        # ROS2
        if self.agent.goal_handle[vehicle_position]:
            self.agent.goal_handle[vehicle_position].cancel_goal_async()
            self.agent.log_info("[ROSHandler] Goal canceled")

    def cancel_goal_waypoints(self, _, vehicle_position):
        # ROS2
        rate = self.agent.node.create_rate(1)
        if self.agent.goal_handle_waypoints[vehicle_position]:
            self.agent.goal_handle_waypoints[vehicle_position].cancel_goal_async()
            while self.agent.task_started[vehicle_position] == 1:
                rate.sleep()
            self.agent.log_info("[ROSHandler] Goal Waypoints Canceled")
            self.agent.waypoints[vehicle_position] = False

    def slow_down_handler(self, _, data: dict, vehicle: str):
        """Handles slowdown messages and sends to specific vehicle parameters"""

        if self.agent.node.traffic_control:
            max_vel_x = 0 # data["data"]["max_vel_x"] # in ROS1 these values are explicitly set to 0
            max_vel_theta = 0 # data["data"]["max_vel_theta"] # in ROS1 these values are explicitly set to 0
            vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
            self.agent.log_info(
                f"[ROSHandler] Slow down requested, collision situation detected for vehicle: {self.agent.vehicle_names[vehicle_position]}")
            # ROS2
            self.agent.send_change_speed_request(
                max_vel_x, max_vel_theta, vehicle_position
            )

    def collision_clearance_handler(self, collision_clearance_msg, data: dict, vehicle: str):
        """Handles clear collision messages and sends to specific vehicle parameters"""
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        msg_type = collision_clearance_msg.message_type
        if msg_type== "slow_down":
            self.agent.log_info(f"[ROSHandler] Collision clearance, restarting vehicle: {self.agent.vehicle_names[vehicle_position]}")
            
            # ROS2
            self.agent.send_change_speed_request(
                self.agent.max_vel_x[vehicle_position], self.agent.max_vel_theta[vehicle_position], vehicle_position
            )

        # elif data["data"]["message_type"] == "path_rerouting":
        #    path_data = {"path": None, "rotation_angles": None}
        #    vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        #    self.agent.log_info(
        #        f"[ROSHandler] Path Rerouting Clearance for vehicle: {self.agent.vehicle_names[vehicle_position]}")
        #    path_data["rotation_angles"] = [data["data"]["task_data"]["location"]["rotation"]]
        #    metric = data["data"]["task_data"]["location"]["metric"]
        #    path_data["path"] = [[metric["x"], metric["y"]]]
        #    self.path_rerouting_handler(_, path_data, vehicle)

    def docking_routine_request_handler(self, data: dict, vehicle: str):
        self.agent.log_info("[ROSHandler] Docking Routine RECORDING request")
        try:
            vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
            vehicle = self.agent.vehicles[vehicle_position]
            vehicle_prefix = str(vehicle["prefix"])

            if vehicle_prefix == "None":
                vehicle_prefix = ""

            # ROS2
            self.client[vehicle_position] = ActionClient(self.agent.node, Docking, "/docking")
            self.agent.log_info("[ROSHandler] Waiting for docking server")
            self.client[vehicle_position].wait_for_server()

            goal = Docking.Goal()
            goal.vehicle_position = vehicle_position
            goal.frequency = 30
            string = String()
            string.data = vehicle_prefix
            goal.vehicle_prefix = string
            self.agent.log_info("Before sending goal")

            future = self.client[vehicle_position].send_goal_async(goal)
            rclpy.spin_until_future_complete(self.client[vehicle_position], future)

        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Error on Docking Routine Request: {e}")

    def topic_handler(self, _, topics, vehicle):
        """find vehicle token to passed to the topic streaming node"""

        if topics and vehicle:

            # initialise custom msg to send topic info

            for index in topics["data"]:
                self.agent.total_topics += 1
                index["vehicle_uuid"] = vehicle

                msg = parse_topic(index, self.agent)  # ROS2 Message Type

                self.agent.topic_streaming_talker(msg)
                self.agent.log_info(
                    f"[ROSHandler] Topic to stream {index['topic']}, for vehicle {vehicle}"
                )
        else:
            self.agent.log_info("[ROSHandler] No topics to stream")

    def end_recording_publisher(self, vehicle_prefix):
        # ROS2
        pub = self.agent.node.create_publisher(
            vehicle_prefix + "/recording", Bool, queue_size=5
        )
        return pub

    def pause_task_handler(self, pause_task_msg, vehicle: str):
        """ Set max_vel_x and max_vel_theta to 0 to pause a task"""
        
        max_vel_x = 0 
        max_vel_theta = 0 
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        self.agent.log_info(
            f"[ROSHandler] Pause task requested for vehicle: {self.agent.vehicle_names[vehicle_position]}")
        # ROS2
        self.agent.send_change_speed_request(
            max_vel_x, max_vel_theta, vehicle_position
        )
    
    def resume_task_handler(self, pause_task_msg, vehicle: str):
        """ Stop max_vel_x and max_vel_theta to default values to resume a task"""
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        self.agent.log_info(f"[ROSHandler] Resume task requested for vehicle: {self.agent.vehicle_names[vehicle_position]}")
        
        # ROS2
        self.agent.send_change_speed_request(
            self.agent.max_vel_x[vehicle_position], self.agent.max_vel_theta[vehicle_position], vehicle_position
        )
        
    def remove_vehicle_from_fleet_handler(self, vehicle: str):
        """ Remove vehicle from fleet """
        self.agent.log_info(f"[ROSHandler] Removing vehicle {vehicle} from fleet")
        # ROS2
        self.agent.remove_vehicle_from_fleet(vehicle)
        
    def set_initial_position_handler(self, vehicle: str, pose_msg: dict):
        """
        Set estimated pose for a vehicle, pose must
        be a dictionary with three keys: {"x", "y", "yaw"}
        """
        self.agent.log_info(f"[ROSHandler] Setting estimated pose for {vehicle}")

        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        self.agent.set_initial_pose(vehicle_position, pose_msg["x"], pose_msg["y"], pose_msg["yaw"])

    def update_map_handler(self, data: dict, vehicle: str, status):
        self.agent.node.get_logger().info("[ROS Handler] Received map update message")
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        vehicle = self.agent.vehicles[vehicle_position]
        vehicle_prefix = str(vehicle["prefix"])

        if vehicle_prefix == "None":
            vehicle_prefix = ""
        
        change_map_service = str(vehicle_prefix)+"/map_server/load_map"
        # Create the service client for LoadMap
        client = self.agent.node.create_client(LoadMap, change_map_service)
        

        # Wait for the service to be available
        while not client.wait_for_service(timeout_sec=5.0):
            self.agent.node.get_logger().info(f"[ROS Handler] Waiting for {change_map_service} service to be available...")

        self.agent.log_info(f"[ROS Handler] Map Update Handler {change_map_service}")

        # Prepare the map files using the data provided
        map_url = prepare_map_files(data.displayable_image, data.yaml_file)

        # Prepare the service request
        request = LoadMap.Request()
        request.map_url = map_url

        try:
            self.agent.node.get_logger().info(f"Waiting for service '{change_map_service}' to be available...")
            while not client.wait_for_service(timeout_sec=10.0):
                self.agent.node.get_logger().warn(f"Service '{change_map_service}' not available, waiting...")

            # Call the service asynchronously and wait for the result
            future = client.call_async(request)
            future.add_done_callback(self.handle_service_response)

        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Change Map Service call failed: {e}")
            return

    
    def handle_service_response(self, future):
        try:
            # Check the result of the future
            if future.result() is not None:
                result = future.result().result
                self.agent.node.get_logger().info(f"Map loaded successfully: {result}")
            else:
                self.agent.node.get_logger().error(f"[ROSHandler] Change Map Service call failed: {future.exception()}")
        except Exception as e:
            self.agent.node.get_logger().error(f"[ROSHandler] Error handling service response: {e}")
        
        # Handle the service result codes
        if result == 0:
            self.agent.log_info("[ROSHandler] Change Map Service: SUCCESS")
        elif result == 1:
            self.agent.log_info("[ROSHandler] Change Map Service: MAP DOES NOT EXIST")
        elif result == 2:
            self.agent.log_info("[ROSHandler] Change Map Service: INVALID MAP DATA")
        elif result == 3:
            self.agent.log_info("[ROSHandler] Change Map Service: INVALID MAP METADATA")
        elif result == 255:
            self.agent.log_info("[ROSHandler] Change Map Service: UNDEFINED FAILURE")
