import rclpy
from rclpy.action import ActionServer
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from meili_agent.logging import Logging

from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Path
from std_msgs.msg import Bool

from meili_ros_lib.docking import DockingActionServer
from action_docking.action import Docking


class DockingActionServerROS2(Node, DockingActionServer):
    def __init__(self, name):
        Node.__init__(self, "docking_server")
        DockingActionServer.__init__(self, name)

        logging = Logging(self)
        self.log_info = logging.log_info
        self.log_error = logging.log_error

        self._as = ActionServer(self, Docking, self.action_name, self.execute_cb)
        self.log_info("[DockingServer] Docking Server Initialized")

    def execute_cb(self, goal):
        self.plan = Path()
        self.recording = True
        self.log_info("[DockingServer] Executing recording callback")
        self.subscribers(goal)

        success = False
        while not success:
            # self.log_info(f"[NOT success] {self.recording == False}")
            # if self._as.is_preempt_requested():
            #     self.log_info(f"[DockingServer] {self._action_name} preempted")
            #     self._as.set_preempted()
            #     break
            if not self.recording:
                success = True

        if success:
            self._result.plan = self.plan
            self.log_info(f"[DockingServer] {self._action_name} succeeded")
            goal.succeed()

    def subscribers(self, goal):
        vehicle_prefix = goal.request.vehicle_prefix.data  # ROS2
        try:

            self.create_subscription(
                vehicle_prefix + "/amcl_pose",
                PoseWithCovarianceStamped,
                self.callback_pose,
                qos_profile=qos_profile_sensor_data,

            )

            self.create_subscription(
                vehicle_prefix + "/recording",
                Bool,
                self.callback_recording,
                qos_profile=qos_profile_sensor_data,
            )

        except Exception as e:
            self.log_error(f"[DockingServer] Subscriber error: {e}")


def main(args=None):
    rclpy.init(args=args)
    docking_server = DockingActionServerROS2("docking")
    rclpy.spin(docking_server)


if __name__ == "__main__":
    main()
