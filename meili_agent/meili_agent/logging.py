
class Logging:

    def __init__(self,node):
        self.node = node

    def log_info(self, message):
        self.node.get_logger().info(message)

    def log_error(self, message):
        self.node.get_logger().error(message)